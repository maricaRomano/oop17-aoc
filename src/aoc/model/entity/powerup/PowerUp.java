package aoc.model.entity.powerup;

public interface PowerUp {
    
    /*
     * The powerup's effect
     */
    public void effect();
    
}
