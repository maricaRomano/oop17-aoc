package aoc.model.level;

import aoc.model.LevelProxy;
import aoc.utilities.ObjectObserver;

/**
 * This interface describe a generic Level of the game.
 */
public interface Level extends LevelProxy, ObjectObserver {

}
