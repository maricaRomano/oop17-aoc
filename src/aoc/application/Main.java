package aoc.application;

import aoc.view.View;
import javafx.application.Application;

/**
 * Main class of the application.
 * 
 */
public final class Main {

    private Main() {
    }

    /**
     * Launch application.
     * 
     * @param args
     *            arguments
     */
    public static void main(final String[] args) {
        Application.launch(View.class, args);
    }

}
