package aoc.view.gameview;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import aoc.controller.Controller;
import aoc.utilities.Input;
import aoc.utilities.Vector;
import aoc.view.menu.controller.GamePaths;
import aoc.view.menu.controller.MainWindow;
import aoc.view.menu.controller.SceneFactory;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;

/**
 * This class implements the methods of GameSceneRender.
 *
 */
public abstract class AbstractGameView implements GameSceneRender {

    /**
     * Constants for sprites positioning.
     */
    private static final int POSITION_ZERO_X = 100;
    private static final int POSITION_ZERO_Y = 20;

        /**
         * The scene where the game takes place.
         */
	protected Scene scene;
	private Image background;
	/**
	 * An handler that collects the player's inputs.
	 */
	public static final EventHandler<KeyEvent> inputHandler = new EventHandler<KeyEvent>() {
	        public void handle(final KeyEvent event) {
	                switch (event.getCode()) {
	                case UP:
	                        InputManager.getSingleton().addInput(Input.UP);
	                        break;
	                case DOWN:
	                        InputManager.getSingleton().addInput(Input.DOWN);
	                        break;
	                case Z:
	                        InputManager.getSingleton().addInput(Input.RAPID_SHOT);
	                        break;
	                case P:
	                        InputManager.getSingleton().addInput(Input.PAUSE);
	                        break;
	                default:
	                        break;
	                }
	        }
	};
	/**
	 * A variable that handles the turbo button.
	 */
	public static final EventHandler<KeyEvent> turboHandler = new EventHandler<KeyEvent>() {
	        public void handle(final KeyEvent event) {
	                switch (event.getCode()) {
	                case X:
	                        InputManager.getSingleton().addInput(Input.SHOT);
	                        break;
	                default:
	                        break;
	                }
	        }
	};
	
	/**
	 * Gets the only instance of SceneFactory.
	 */
        protected static final SceneFactory sf = MainWindow.getSingleton().getFactory();

        /**
         * The canvas where the Controller draws the characters' sprites.
         */
	@FXML
	protected Canvas canvas;
	/**
	 * A label that shows the level the user is playing.
	 */
	@FXML
	protected Label numberLevel;
	
	/**
	 * Constructor that returns to the Controller a new game view.
	 */
	protected AbstractGameView() {
		Controller.get().setGameView(this);
	}

	/**
	 * Initialize the game scene with the background.
	 */
	@Override
	public void init() {
	    background = new Image(this.getClass().getResourceAsStream(GamePaths.IMAGES_BACKGROUND_FOLDER
	            +"background.jpg"));
	    this.canvas.getGraphicsContext2D().drawImage(background, 0, 0);
	}

	/**
	 * Method that allows to draw the sprites on the canvas.
	 */
	@Override
	public void draw(final Image i, final Vector position) {
		this.canvas.getGraphicsContext2D().drawImage(i, 
		        POSITION_ZERO_X + position.getX() * 2, POSITION_ZERO_Y + position.getY() * 2,
			i.getHeight() / 3, i.getWidth() / 3);
	}
	
	/**
	 * It collects the inputs for the Controller.
	 */
	@Override
	public List<Input> getInput() {
	    List<Input> list = Collections.unmodifiableList(InputManager.getSingleton().getInputs());
	    InputManager.getSingleton().clearBuffer();
	    return list;
	}

	/**
	 * Shows a alert window that indicate the player's success.
	 */
	@Override
	public void win() {
		Alert winAlert = new Alert(AlertType.INFORMATION);
		winAlert.setTitle("You win!");
		winAlert.setHeaderText(null);
		winAlert.setContentText("Great job! But Mama will bake again tomorrow...");

		Optional<ButtonType> confirm = winAlert.showAndWait();
	            if (confirm.get() == ButtonType.OK) {
	                MainWindow.getSingleton().click();
	                MainWindow.getSingleton().getSoundManager().clickBackToMainMenu();
	                this.returnMainMenu();
	            }
	}

	/**
	 * Shows a alert window that indicate the player's failure.
	 */
	@Override
	public void lost() {
		Alert lostAlert = new Alert(AlertType.INFORMATION);
		lostAlert.setTitle("You lost...");
		lostAlert.setHeaderText(null);
		lostAlert.setContentText("Oh no! The kids reached the cake! Mama is not happy... :(");

		Optional<ButtonType> confirm = lostAlert.showAndWait();
	            if (confirm.get() == ButtonType.OK) {
	                MainWindow.getSingleton().click();
	                MainWindow.getSingleton().getSoundManager().clickBackToMainMenu();
	                this.returnMainMenu();
	            }
	}

	/**
	 * Set the game in pause and show an alert dialog with two choices: resume the game or exit.
	 */
	@Override
	public void pause() {
	    Controller.get().pause();
	    Alert pauseAlert = new Alert(AlertType.CONFIRMATION);
	    pauseAlert.setTitle("Game paused");
	    pauseAlert.setHeaderText(null);
	    pauseAlert.setContentText("Do you want to exit?");

	    Optional<ButtonType> confirm = pauseAlert.showAndWait();
	    if (confirm.get() == ButtonType.OK) {
	       MainWindow.getSingleton().click();
	       MainWindow.getSingleton().getSoundManager().clickBackToMainMenu();
	       this.returnMainMenu();
	       Controller.get().end();
	    } else {
	    	Controller.get().proceed();
	    }
	}

        /**
         * To get and show scenes.
         * @param event
         */
        private void showScene() {
            MainWindow.getSingleton().click();
            Scene scene = sf.getScene();
            MainWindow.getSingleton().getWindow().setScene(scene);
            MainWindow.getSingleton().getWindow().show();
        }

        /**
         * Method that allows to back to MainMenu.
         */
        private void returnMainMenu() {
            MainWindow.getSingleton().click();
            try {
                sf.setScene("MainMenu.fxml");
                showScene();
            } catch (IOException e) {
                e.printStackTrace();
            }
            showScene();
        }
}
