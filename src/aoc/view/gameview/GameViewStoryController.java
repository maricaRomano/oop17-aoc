package aoc.view.gameview;

import javafx.scene.canvas.Canvas;

/**
 * This class extends AbstractGameView and has a method to set the label 
 * in order to show the level the user is playing.
 *
 */
public class GameViewStoryController extends AbstractGameView {

        /**
         * An empty constructor.
         */
	public GameViewStoryController() {

	}
	
	/**
	 * The method that sets the label.
	 * @param index
	 *          level's number
	 */
	public void setLabel(final int index) {
	    this.numberLevel.setText(String.valueOf(index));
	}
	
	/**
	 * Getter that picks the canvas of the class.
	 * @return canvas
	 */
	public Canvas getCanvas() {
	    return this.canvas;
	}
	
}
