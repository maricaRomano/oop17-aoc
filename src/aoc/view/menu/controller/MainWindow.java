package aoc.view.menu.controller;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * The main class for the view creation.
 * 
 */

public final class MainWindow extends Application {

	private Stage window;
	private SceneFactory sf = new SceneFactoryImpl();
	private SoundManager sm = new SoundManagerImpl(); 
	
	/**
	 * It contains the SINGLETON, initialized at first use.
         */
        private static class LazyHolder {
            /**
             * Contains the reference to the Singleton.
             */
            private static final MainWindow SINGLETON = new MainWindow();
        }

        private MainWindow() {

        }
	
	@Override
	public void start(final Stage primarystage) throws Exception {
		window = primarystage;
		sf.setScene("MainMenu.fxml");
		Scene scene = sf.getScene();	
        window.setWidth(Utils.STAGE_WIDTH);
        window.setHeight(Utils.STAGE_HEIGHT);
        window.setResizable(false);
        window.setTitle("Attack On Cake!!");
        window.getIcons().add(new Image(this.getClass().getResourceAsStream(GamePaths.IMAGES_FOLDER + "Logo.png")));
        window.setScene(scene);
        window.show();
        window.setOnCloseRequest(e -> {
        	e.consume();
        	System.exit(1);
            });
	}
	
	/**
	 * Getter for the SceneFactory.
	 * 
	 * @return SceneFactory
	 */
    public SceneFactory getFactory() {
    	return sf;
    }

    /**
     * getter for the SoundManager.
     * 
     * @return SoundManager
     */
    public SoundManager getSoundManager() {
    	return sm;
    }

    /**
     * To play the button sound.
     */
    public void click() {
        sm.click();
    }

    /**
     * Returns this stage.
     * 
     * @return window
     *          this stage
     */
    public Stage getWindow() {
        return window;
    }

    /**
     * Singleton method to get the instance of this class.
     * 
     * @return the only instance of this class
     */
    public static MainWindow getSingleton() {
        return LazyHolder.SINGLETON;
    }
}
