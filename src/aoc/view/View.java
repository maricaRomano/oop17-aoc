package aoc.view;

import aoc.view.menu.controller.MainWindow;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The class that sets the Main Menu.
 *
 */
public class View extends Application {

    @Override
    public final void start(final Stage primaryStage) throws Exception {
        MainWindow.getSingleton().start(primaryStage);
    }
}
