package aoc.controller.gameloop;

/**
 * The interface for the gameloop, which implements the gameloop proxy.
 */
public interface GameLoopInterface extends GameLoopProxy {

}
