package aoc.controller;

import aoc.controller.gameloop.GameLoop.Status;
import aoc.controller.gameloop.GameLoop;
import aoc.controller.gameloop.GameLoopInterface;
import aoc.controller.gameloop.GameLoopProxy;
import aoc.view.gameview.GameSceneRender;
import java.util.Optional;

/**
 * This class represents the main object Controller,using the Singleton Pattern.
 * It contains a reference to the DataManager and the eventual GameLoop.
 */
public class Controller implements GameLoopProxy {
    
    /**
     * Gameloop of the current level.
     */
    private Optional<GameLoopInterface> gameloop;
    
    /**
     * Current setting for fps.
     */
    private int fps;
    
    /**
     * Gameview of the current level.
     */
    private Optional<GameSceneRender> gameview = Optional.empty();
    
    /**
     * It contains the SINGLETON, initialized at first use.
     */
    private static class LazyHolder {
	/**
	 * Contains the reference to the Singleton.
	 */
	private static final Controller SINGLETON = new Controller();
    }
    
    /**
     * Controller Constructor.
     */
    private Controller() {
    	fps = GameConstants.FPS_30;
    	gameloop = Optional.empty();
    }
    
    /**
     * Get the reference to the SINGLETON.
     * @return the SINGLETON
     */
    public static Controller get() {
	return LazyHolder.SINGLETON;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void end() {
	this.applyMethod("end");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void pause() {
	this.applyMethod("pause");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void proceed() {
	this.applyMethod("proceed");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Status getStatus() {
	try {
	    checkCondition(gameloop.isPresent(), "No level is being played");
	    return gameloop.get().getStatus();
	} catch (IllegalArgumentException e) {
	    e.printStackTrace();
	}
	return null;
    }
    
    /**
     * Starts the GameLoop for the desired level.
     * @param index
     *          Index of the level to play
     */
    public void start(final Optional<Integer> index) {
	try {
	    if (index.isPresent()) {
		checkCondition(index.get() > 0
		        && index.get() <= GameConstants.N_LEVELS, "Level requested doesn't exist");
	    }
	    checkCondition(gameview.isPresent(), "The view is not ready");
	    final GameLoop temp = new GameLoop(index, gameview.get());
	    gameloop = Optional.of(temp);
	} catch (IllegalArgumentException e) {
	    e.printStackTrace();
	}
    }
    
    /**
     * Returns the current setting for the FPS.
     * @return int
     */
    public int getFPS() {
	return fps;
    }

    /**
     * Sets the FPS.
     * @param setting
     *          The desired setting.
     */
    public void setFPS(final int setting) {
	try {
	    checkCondition(GameConstants.FPS_SETTINGS.contains(setting), "This option isn't available");
	    fps = setting;
	 } catch (IllegalArgumentException e) {
	     e.printStackTrace();
	}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Integer> getLevel() {
	try {
	    checkCondition(gameloop.isPresent(), "No level is being played");
	    return gameloop.get().getLevel();
	} catch (IllegalArgumentException e) {
	    e.printStackTrace();
	}
	return Optional.empty();
    }
    
    /**
     * This method check if a condition is true;
     * if not it throws an Exception with the message passed as argument.
     * @param supplier
     *          the condition to check.
     * @param message
     *          the String of the eventual error
     * @throws IllegalArgumentException
     *          It is thrown in case the supplier is false.
     */
    private void checkCondition(final boolean supplier, final String message) throws IllegalArgumentException {
	if (!supplier) {
	    throw new IllegalArgumentException(message);
	}
    }
    
    /**
     * Calls a method on the GameLoop object if present.
     * @param methodName
     *          the name of the method of GameLoopInterface
     */
    private void applyMethod(final String methodName) {
	try {
	    checkCondition(gameloop.isPresent(), "No level is being played");
	    GameLoop.class.getMethod(methodName).invoke(gameloop.get());
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
    /**
     * This method sets the gameview where the level will be drawn.
     * @param GameView
     *          Object controlling the view.
     */
    public void setGameView(final GameSceneRender gameview) {
            this.gameview = Optional.of(gameview);
    }
}
