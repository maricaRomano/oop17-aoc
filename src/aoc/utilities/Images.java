package aoc.utilities;

import javafx.scene.image.Image;

import aoc.view.menu.controller.GamePaths;

/**
 * Utility class that loads the characters sprites.
 *
 */
public enum Images { 

    /**
     * Gets Mother sprite.
     */
    MOTHER(GamePaths.IMAGES_MOTHER_FOLDER + "motherDraft.gif"),
    /**
     * Gets Dumb Child sprite.
     */
    DUMB_CHILD(GamePaths.IMAGES_CHILD_FOLDER + "dumb_child.gif"),
    /**
     * Gets Fat Child sprite.
     */
    FAT_CHILD(GamePaths.IMAGES_CHILD_FOLDER + "fat_child.gif"),
    /**
     * Gets Nerd Child sprite.
     */
    NERD_CHILD(GamePaths.IMAGES_CHILD_FOLDER + "nerd_child.gif"),
    /**
     * Gets Athletic Child sprite.
     */
    ATHLETIC_CHILD(GamePaths.IMAGES_CHILD_FOLDER + "athletic_child.gif"),
    /**
     * Gets Rich Child sprite.
     */
    RICH_CHILD(GamePaths.IMAGES_CHILD_FOLDER + "rich_child.gif"),
    /**
     * Gets Slipper sprite.
     */
    SLIPPER(GamePaths.IMAGES_SLIPPER_FOLDER + "slipperDraft.gif");

    private String path;
    private Image image;

    /**
     * Gets the image by his path.
     * @param path
     */
    private Images(final String path) {
	this.path = path;
        try {
        this.image = new Image(this.getClass().getResourceAsStream(path));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Returns the image.
     * @return image
     */
    public Image getImage() {
	return this.image;
    }

    /**
     * Gets the image path.
     * @return path
     */
    public String getPath() {
	return this.path;
    }
}
